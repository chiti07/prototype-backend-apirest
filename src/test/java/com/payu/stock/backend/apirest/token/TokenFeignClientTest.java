package com.payu.stock.backend.apirest.token;

import com.payu.stock.backend.apirest.StockBackendApirestApplication;
import com.payu.stock.backend.apirest.token.models.CreditCardToken;
import com.payu.stock.backend.apirest.token.models.Merchant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(classes = StockBackendApirestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class TokenFeignClientTest {

    @Autowired
    TokenFeignClient tokenFeignClient;

    @Test
    public void should_return_success_when_call_the_network(){
        TokenRequest tokenRequest = TokenRequest.builder()
                .language("es")
                .command("CREATE_TOKEN")
                .merchant(Merchant.builder()
                        .apiLogin("012345678901")
                        .apiKey("012345678901")
                        .build())
                .creditCardToken(CreditCardToken.builder()
                        .payerId("10")
                        .name("full name")
                        .identificationNumber("32144457")
                        .paymentMethod("VISA")
                        .number("4111111111111111")
                        .expirationDate("2022/01")
                        .build())
                .build();
        Map<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");

        TokenResponse tokenResponse = tokenFeignClient.createToken(header,tokenRequest);
        assertThat(tokenResponse).satisfies(response -> {
            assertThat(response).isNotNull();
            assertThat(response.getCode()).isNotNull().isEqualTo("SUCCESS");
        });

    }

}
