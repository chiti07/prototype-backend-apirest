package com.payu.stock.backend.apirest.payment;

import com.payu.stock.backend.apirest.StockBackendApirestApplication;
import com.payu.stock.backend.apirest.payment.models.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = StockBackendApirestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class PaymentFeignClientTest {
    @Autowired
    PaymentFeignClient paymentFeignClient;

    @Test
    public void should_return_success_when_doing_transaction(){
        TransactionRequest transactionRequest = TransactionRequest.builder()
                .language("es")
                .command("SUBMIT_TRANSACTION")
                .merchant(Merchant.builder()
                        .apiKey("4Vj8eK4rloUd272L48hsrarnUA")
                        .apiLogin("pRRXKOl8ikMmt9u")
                        .build())
                .transaction(Transaction.builder()
                        .order(Order.builder()
                                .accountId("512321")
                                .referenceCode("TestPayU")
                                .description("payment test")
                                .language("es")
                                .signature("7ee7cf808ce6a39b17481c54f2c57acc")
                                .notifyUrl("http://www.tes.com/confirmation")
                                .additionalValues(AdditionalValues.builder()
                                        .txValue(TxValue.builder()
                                                .value(20000)
                                                .currency("COP")
                                                .build())
                                        .txTax(TxTax.builder()
                                                .value(3193)
                                                .currency("COP")
                                                .build())
                                        .txTaxReturnBase(TxTaxReturnBase.builder()
                                                .value(16806)
                                                .currency("COP")
                                                .build())
                                        .build())
                                .buyer(Buyer.builder()
                                        .merchantBuyerId("1")
                                        .fullName("First name and second buyer  name")
                                        .emailAddress("buyer_test@test.com")
                                        .contactPhone("7563126")
                                        .dniNumber("5415668464654")
                                        .shippingAddress(ShippingAddress.builder()
                                                .street1("calle 100")
                                                .street2("5555487")
                                                .city("Medellin")
                                                .state("Antioquía")
                                                .country("CO")
                                                .postalCode("000000")
                                                .phone("7563126")
                                                .build())
                                        .build())
                                .shippingAddress(ShippingAddress.builder()
                                        .street1("calle 100")
                                        .street2("5555487")
                                        .city("Medellin")
                                        .state("Antioquía")
                                        .country("CO")
                                        .postalCode("000000")
                                        .phone("7563126")
                                        .build())
                                .build())
                        .payer(Payer.builder()
                                .merchantPayerId("1")
                                .fullName("First name and second buyer  name")
                                .emailAddress("buyer_test@test.com")
                                .contactPhone("7563126")
                                .dniNumber("5415668464654")
                                .billingAddress(BillingAddress.builder()
                                        .street1("calle 100")
                                        .street2("5555487")
                                        .city("Medellin")
                                        .state("Antioquía")
                                        .country("CO")
                                        .postalCode("000000")
                                        .phone("7563126")
                                        .build())
                                .build())
                        .creditCard(CreditCard.builder()
                                .number("4097440000000004")
                                .securityCode("321")
                                .expirationDate("2021/12")
                                .name("REJECTED")
                                .build())
                        .extraParameters(ExtraParameters.builder()
                                .installments_number(1)
                                .build())
                        .type("AUTHORIZATION_AND_CAPTURE")
                        .paymentMethod("MASTERCARD")
                        .paymentCountry("CO")
                        .deviceSessionId("vghs6tvkcle931686k1900o6e1")
                        .ipAddress("127.0.0.1")
                        .cookie("pt1t38347bs6jc9ruv2ecpv7o2")
                        .userAgent("Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0")
                        .build())
                .test(false)
                .build();

        Map<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");

        TransactionResp transactionResp = paymentFeignClient.makeTransaction(header, transactionRequest);
        assertThat(transactionResp).satisfies(response -> {
            assertThat(response).isNotNull();
            assertThat(response.getCode()).isNotNull().isEqualTo("SUCCESS");
        });
    }
}
