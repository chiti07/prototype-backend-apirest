package com.payu.stock.backend.apirest.payment;

import com.payu.stock.backend.apirest.StockBackendApirestApplication;
import com.payu.stock.backend.apirest.payment.models.Merchant;
import com.payu.stock.backend.apirest.payment.models.Order;
import com.payu.stock.backend.apirest.payment.models.Transaction;
import com.payu.stock.backend.apirest.payment.refund.RefundFeignClient;
import com.payu.stock.backend.apirest.payment.refund.RefundRequest;
import com.payu.stock.backend.apirest.payment.refund.RefundResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = StockBackendApirestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class RefundFeignClientTest {

    @Autowired
    RefundFeignClient refundFeignClient;

    @Test
    public void should_return_error_when_the_refund_was_already_done(){
        RefundRequest refundRequest = RefundRequest.builder()
                .language("es")
                .command("SUBMIT_TRANSACTION")
                .merchant(Merchant.builder()
                        .apiKey("4Vj8eK4rloUd272L48hsrarnUA")
                        .apiLogin("pRRXKOl8ikMmt9u")
                        .build())
                .transaction(Transaction.builder()
                        .order(Order.builder()
                                .id("121186386")
                                .build())
                        .type("REFUND")
                        .reason("Reason for requesting the refund or cancellation of the transaction")
                        .parentTransactionId("b0df595b-707d-4519-82a2-5e20684905a6")
                        .build())
                .test(true)
                .build();

        Map<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");

        RefundResponse refundResponse = refundFeignClient.refundOrder(header,refundRequest);
        assertThat(refundResponse).satisfies(response -> {
            assertThat(response).isNotNull();
            assertThat(response.getError()).isNotNull().isEqualTo("No es posible procesar la solicitud. Existen solicitudes de cancelación en proceso.");
        });
    }

    @Test
    public void should_return_success_when_doing_a_firts_refund_attempt(){
        RefundRequest refundRequest = RefundRequest.builder()
                .language("es")
                .command("SUBMIT_TRANSACTION")
                .merchant(Merchant.builder()
                        .apiKey("4Vj8eK4rloUd272L48hsrarnUA")
                        .apiLogin("pRRXKOl8ikMmt9u")
                        .build())
                .transaction(Transaction.builder()
                        .order(Order.builder()
                                .id("121186800")
                                .build())
                        .type("REFUND")
                        .reason("Reason for requesting the refund or cancellation of the transaction")
                        .parentTransactionId("02836c84-6561-4b3f-8a74-7b6fe616d32b")
                        .build())
                .test(true)
                .build();

        Map<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");

        RefundResponse refundResponse = refundFeignClient.refundOrder(header,refundRequest);
        assertThat(refundResponse).satisfies(response -> {
            assertThat(response).isNotNull();
            assertThat(response.getCode()).isNotNull().isEqualTo("SUCCESS");
        });
    }
}
