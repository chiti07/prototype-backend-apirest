package com.payu.stock.backend.apirest.auth;

import com.payu.stock.backend.apirest.models.entity.User;
import com.payu.stock.backend.apirest.user.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class AditionalTokenInfo implements TokenEnhancer {

    @Autowired
    private UserServiceInterface userServiceInterface;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        User user = userServiceInterface.findByUsername(oAuth2Authentication.getName());
        Map<String, Object> info = new HashMap<>();
        info.put("additional_info", "userinfo".concat(oAuth2Authentication.getName()));
//      info.put("username", user.getId() + ": " + user.getUsername());
        info.put("name", user.getId() + ": " + user.getName());
        info.put("last_name", user.getId() + ": " + user.getLast_name());
        info.put("email", user.getId() + ": " + user.getEmail());

        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(info);
        return oAuth2AccessToken;
    }
}
