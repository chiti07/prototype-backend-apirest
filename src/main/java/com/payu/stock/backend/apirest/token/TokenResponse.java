package com.payu.stock.backend.apirest.token;

import com.payu.stock.backend.apirest.token.models.CreditCardToken;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TokenResponse {
    private String code;
    private String error;
    private CreditCardToken creditCardToken;
}
