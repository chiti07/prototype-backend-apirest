package com.payu.stock.backend.apirest.stock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class UploadFileImp implements UploadFile{

    private final Logger log = LoggerFactory.getLogger(UploadFileImp.class);

    private final static String UPLOAD_DIRECTORY= "/Users/juanluis/eclipse-workspace/stock-backend-apirest/uploads";

    @Override
    public Resource load(String pictureName) throws MalformedURLException {
        Path filePath = getPath(pictureName);
        log.info(filePath.toString());

        Resource resource = new UrlResource(filePath.toUri());

        if (!resource.exists() && !resource.isReadable()){
            filePath = Paths.get("src/main/resources/img").resolve("no-img.png").toAbsolutePath();
                resource = new UrlResource(filePath.toUri());
            log.error("Error, there was a problem uploading the picture: " + pictureName);
        }
        return  resource;
    }

    @Override
    public String copy(MultipartFile file) throws IOException{
        String fileName = UUID.randomUUID().toString()+ "_" + file.getOriginalFilename().replace(" ", "");
        Path filePath = getPath(fileName);
        log.info(filePath.toString());

            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);

        return fileName;
    }

    @Override
    public boolean delete(String pictureName) {

        if (pictureName != null && pictureName.length() > 0){
            Path pathPreviousPicture = Paths.get("/Users/juanluis/eclipse-workspace/stock-backend-apirest/uploads").resolve(pictureName).toAbsolutePath();
            File previousPictureFile = pathPreviousPicture.toFile();
            if (previousPictureFile.exists() && previousPictureFile.canRead()){
                previousPictureFile.delete();

                return true;
            }
        }

        return false;
    }

    @Override
    public Path getPath(String pictureName) {
        return Paths.get(UPLOAD_DIRECTORY).resolve(pictureName).toAbsolutePath();
    }
}
