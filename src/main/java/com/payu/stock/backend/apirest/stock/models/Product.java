package com.payu.stock.backend.apirest.stock.models;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
@Builder
public class Product {

    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer amount;
    private String category;
    private Date lastUpdated;
    private String picture;



}
