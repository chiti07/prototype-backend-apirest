package com.payu.stock.backend.apirest.models.repository.user;

import com.payu.stock.backend.apirest.models.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    public User findByUsername(String username);
}
