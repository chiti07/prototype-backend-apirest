package com.payu.stock.backend.apirest.auth;

public class JwtConfig {
    public static final String SECRET_KEY = "secret.key.123456789";

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEAr2lPm1c40RYx1NPdOewUifcD9z9zGXBbk4fCc5WNkFMBOw4l\n" +
            "GS/lsHp7gwQzEO+NK9UUPbK6WlggYviA0Yw1jfmZUfJnPMdXtRhscIWM02Dq/n8o\n" +
            "nCH+itKV3bcXa1Hxtj5uUgA+J6T88oxCgXXPlm6Edog+cgyHwFcrqgDVkihnVv0K\n" +
            "S9b83R8+v3DLC70svWODOr/PhN5iha4+6C055D3crTt8CxtqnW3ZM9u1qTX9pboE\n" +
            "bEAgt8wTLkJUAWD0QGExai1XWZmfOe+8j8AvALrptP9h2LEK++8jIcvcQ60TRKEB\n" +
            "sjoDfvMNMW5q+m/ecxdjvxWxhKBxbICM8/2zIwIDAQABAoIBAQCPgHkm3WlW7HBh\n" +
            "692NWEzJPbMZ3+eaxSe1/xeelZfVnYJhPMYu39dp1Bodah8Y2NFGLbC8dO4toPEF\n" +
            "5sUEKU9a0fqTZtr8hxyCw/Na7nbyKtS01XR0nv7ttS/zzai0HLrA3DxGi3pN1pU1\n" +
            "HHgAD1B/gfSOQBHOHeAlA2tuTfZq9ZYvqO7K+MbuaEcwXriQkj4gahDjKtvr5jSe\n" +
            "RI5hBp3evVehs81QDneGu4NoLhOEsfcplo96VBna3OAZAbr0jceyrT/VVFIQcMNd\n" +
            "JB0oUhqH1UK99mYxIFZtP+qITrdOxeUgccjRYhd6CJ/wahxjF+XkNzDJYpBF85Zk\n" +
            "VBP7k3XxAoGBANXrweN15Ye0ns4+uQgtLumwBqtWXmtdkcZNwX07XkZ52Gvv2T8x\n" +
            "mPt5Ei49XQZZX/rHGg6Qp7lLp7GJwGo/k/bHGZgsaxtas+l0Q6auuNZTZ2QbXIjV\n" +
            "INgJgXaqnCTXP5sR6O0RdTIWJ6heXsuyOj0i5/jdd5FqgS6eSN8fWnJpAoGBANHq\n" +
            "W+2tX7jgycqnAxL9I50iiHvAg3MHMqIIKW5wMBIHojUQd7lhpjCRSi/KDIL6pZJb\n" +
            "2EiNg+tXHrpvXqbKEmoODrHwPUiN9JWdTJQt9DglkhQOAuCeHZ7keZBeo1b3P6pe\n" +
            "szRLJxHDZnqbWT7atnpMRP6nQPsF/M3yqUKUwi+rAoGAFBMJoNTBc1ipxmTwbCoB\n" +
            "SwCvv4hdV+nx8g/95kiZ8jcg7hAV7O40kvSrlhdAEYZ1kUz5zwAYHTVSSUDKA65v\n" +
            "rirE5hjU/D4ov/GfH5cp1UMCYsyGhHirmlpaAc7E1Fg2qdCmcQjAXLDji9M/RhI1\n" +
            "kOMjraDqxYSfFsbXw8hnbnECgYEA0beFXkBfKLKTh/4xSRBUO1Hb718XNJ2VTWJA\n" +
            "GlDFBc5QlRk9s5zGTzFoX7RJqbXGIyHVRC72A/IHBhXsB1BM813V+pnccBc6qxr6\n" +
            "/fOnUUzf019oKoZta2gxtzWFlIXyOkDgYrdOciW4xXdcwzn5Itf9+jiY5/FEb3Em\n" +
            "/xjlpH8CgYAtCespGj/p2F1SQ9bqXnOk8zoihq/gzRoxSxNawfxoqnbGK2JXX2J3\n" +
            "98pnFIoyPcMBpkUE7oxg5KriMZ01epGpiBnC1fKVWb9Y/ltkUq/aCWkzxcHSLsbE\n" +
            "Ut7LBNWMniYzBnrQMHtqmZ/YCUr77SABABvQiyy5NcZpUXt+ItRqSw==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr2lPm1c40RYx1NPdOewU\n" +
            "ifcD9z9zGXBbk4fCc5WNkFMBOw4lGS/lsHp7gwQzEO+NK9UUPbK6WlggYviA0Yw1\n" +
            "jfmZUfJnPMdXtRhscIWM02Dq/n8onCH+itKV3bcXa1Hxtj5uUgA+J6T88oxCgXXP\n" +
            "lm6Edog+cgyHwFcrqgDVkihnVv0KS9b83R8+v3DLC70svWODOr/PhN5iha4+6C05\n" +
            "5D3crTt8CxtqnW3ZM9u1qTX9pboEbEAgt8wTLkJUAWD0QGExai1XWZmfOe+8j8Av\n" +
            "ALrptP9h2LEK++8jIcvcQ60TRKEBsjoDfvMNMW5q+m/ecxdjvxWxhKBxbICM8/2z\n" +
            "IwIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
