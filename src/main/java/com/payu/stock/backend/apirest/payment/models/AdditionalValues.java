package com.payu.stock.backend.apirest.payment.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AdditionalValues {
    @JsonProperty("TX_VALUE")
    private TxValue txValue;
    @JsonProperty("TX_TAX")
    private TxTax txTax;
    @JsonProperty("TX_TAX_RETURN_BASE")
    private TxTaxReturnBase txTaxReturnBase;

}
