package com.payu.stock.backend.apirest.payment;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(
        url="${gymapp.payuClient.url}",
        value="payment"
)
public interface PaymentFeignClient {

    @PostMapping
    TransactionResp makeTransaction(
            @RequestHeader Map<String, String> header,
            @RequestBody TransactionRequest transactionRequest
    );
}
