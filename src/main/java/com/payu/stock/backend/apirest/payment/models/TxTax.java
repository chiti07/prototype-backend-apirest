package com.payu.stock.backend.apirest.payment.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TxTax {
    private Integer value;
    private String currency;
}
