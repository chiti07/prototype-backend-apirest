package com.payu.stock.backend.apirest.stock;

import com.payu.stock.backend.apirest.stock.models.Product;

import java.util.List;

public interface ProductService {
	
	public List<Product> findAll();
	
	public Product update(Product product);
	
	public void delete(Long id);
	
	public Product findByID(Long id);

	public Product create(Product product);



}
