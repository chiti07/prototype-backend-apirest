package com.payu.stock.backend.apirest.payment;

import com.payu.stock.backend.apirest.payment.models.Merchant;
import com.payu.stock.backend.apirest.payment.models.Transaction;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionRequest {
    private String language;
    private String command;
    private Merchant merchant;
    private Transaction transaction;
    private Boolean test;
}
