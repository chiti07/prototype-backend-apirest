package com.payu.stock.backend.apirest.token;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(
        url="${gymapp.payuClient.url}",
        value="token"
)
public interface TokenFeignClient {

    @PostMapping
    TokenResponse createToken(
            @RequestHeader Map<String, String> header,
            @RequestBody TokenRequest tokenRequest
    );


}
