package com.payu.stock.backend.apirest.commons;

import com.payu.stock.backend.apirest.models.entity.ProductEntity;
import com.payu.stock.backend.apirest.stock.models.Product;


public class MapperProduct {
    public static ProductEntity toProductEntity(Product product){
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(product.getId());
        productEntity.setName(product.getName());
        productEntity.setPrice(product.getPrice());
        productEntity.setAmount(product.getAmount());
        productEntity.setCategoryEntity(product.getCategory());
        productEntity.setLastUpdated(product.getLastUpdated());
        productEntity.setDescription(product.getDescription());
        productEntity.setPicture(product.getPicture());

        return productEntity;

    }

    public static Product toProduct(ProductEntity productEntity){
        return Product.builder()
                .id(productEntity.getId())
                .name(productEntity.getName())
                .category(productEntity.getCategoryEntity())
                .amount(productEntity.getAmount())
                .description(productEntity.getDescription())
                .price(productEntity.getPrice())
                .lastUpdated(productEntity.getLastUpdated())
                .picture(productEntity.getPicture())
                .build();



    }
}
