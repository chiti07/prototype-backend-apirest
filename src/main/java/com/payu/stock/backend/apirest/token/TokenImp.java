package com.payu.stock.backend.apirest.token;

import com.payu.stock.backend.apirest.models.entity.TokenEntity;
import com.payu.stock.backend.apirest.models.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.payu.stock.backend.apirest.commons.MapperToken.toTokenEntity;
import static com.payu.stock.backend.apirest.commons.MapperToken.toTokenResponse;


@Service
public class TokenImp  implements TokenFeignClient {

    @Autowired
    private TokenRepository tokenDao;

    @Autowired
    private TokenFeignClient tokenFeignClient;


    @Override
    public TokenResponse createToken(Map<String, String> header, TokenRequest tokenRequest) {

        TokenEntity tokenEntity = new TokenEntity();
        TokenResponse tokenResponse = tokenFeignClient.createToken(header,tokenRequest);
        tokenEntity.setCreditCardTokenId(tokenResponse.getCreditCardToken().getCreditCardTokenId());
        tokenEntity.setCreationDate(tokenResponse.getCreditCardToken().getCreationDate());
        tokenEntity.setErrorDescription(tokenResponse.getCreditCardToken().getErrorDescription());
        tokenEntity.setExpirationDate(tokenResponse.getCreditCardToken().getExpirationDate());
        tokenEntity.setPayerId(tokenResponse.getCreditCardToken().getPayerId());
        tokenEntity.setPaymentMethod(tokenResponse.getCreditCardToken().getPaymentMethod());
        tokenEntity.setMaskedNumber(tokenResponse.getCreditCardToken().getMaskedNumber());
        tokenEntity.setName(tokenResponse.getCreditCardToken().getName());
        tokenEntity.setNumber(tokenResponse.getCreditCardToken().getNumber());
        tokenEntity.setIdentificationNumber(tokenResponse.getCreditCardToken().getIdentificationNumber());

        tokenDao.save(tokenEntity);
        return tokenResponse;


    }






}
