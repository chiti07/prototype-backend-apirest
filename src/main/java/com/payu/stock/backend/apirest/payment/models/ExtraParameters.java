package com.payu.stock.backend.apirest.payment.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonPropertyOrder({
        "INSTALLMENTS_NUMBER"
})
public class ExtraParameters {
    @JsonProperty("INSTALLMENTS_NUMBER")
    private Integer installments_number;

}
