package com.payu.stock.backend.apirest.payment.refund;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(
        url="${gymapp.payuClient.url}",
        value="refund"
)
public interface RefundFeignClient {

    @PostMapping
    RefundResponse refundOrder(
            @RequestHeader Map<String, String> header,
            @RequestBody RefundRequest refundRequest
            );
}
