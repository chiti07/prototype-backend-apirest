package com.payu.stock.backend.apirest.stock;

import com.payu.stock.backend.apirest.commons.MapperProduct;
import com.payu.stock.backend.apirest.stock.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.payu.stock.backend.apirest.models.entity.ProductEntity;
import com.payu.stock.backend.apirest.models.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

import static com.payu.stock.backend.apirest.commons.MapperProduct.toProduct;
import static com.payu.stock.backend.apirest.commons.MapperProduct.toProductEntity;


@Service
public class ProductServiceImp implements ProductService {
	@Autowired
	private ProductRepository productDao;

	@Override
	@Transactional(readOnly = true)
	public List<Product> findAll() {
		return ((List<ProductEntity>) productDao.findAll())
				.stream()
				.map(MapperProduct::toProduct)
				.collect(Collectors.toList());
	}

	@Override
	public Product update(Product product) {
		ProductEntity productEntity = productDao.save(toProductEntity(product));
		return toProduct(productEntity);
	}

	@Override
	public void delete(Long id) {
		productDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Product findByID(Long id) {
		return toProduct(productDao.findById(id).orElse(null));
	}

	@Override
	public Product create(Product product) {
		ProductEntity productEntity = productDao.save(toProductEntity(product));
		return toProduct(productEntity);
	}




}
