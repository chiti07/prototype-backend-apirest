package com.payu.stock.backend.apirest.models.entity;


import com.payu.stock.backend.apirest.token.TokenResponse;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name="token")
public class TokenEntity {

    private String payerId;
    private String name;
    private String identificationNumber;
    private String paymentMethod;
    private String number;
    private String expirationDate;
    @Id
    private String creditCardTokenId;
    private String creationDate;
    private String maskedNumber;
    private String errorDescription;


    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCreditCardTokenId() {
        return creditCardTokenId;
    }

    public void setCreditCardTokenId(String creditCardTokenId) {
        this.creditCardTokenId = creditCardTokenId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getMaskedNumber() {
        return maskedNumber;
    }

    public void setMaskedNumber(String maskedNumber) {
        this.maskedNumber = maskedNumber;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
