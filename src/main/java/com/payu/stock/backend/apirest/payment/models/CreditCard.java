package com.payu.stock.backend.apirest.payment.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreditCard {
    private String number;
    private String securityCode;
    private String expirationDate;
    private String name;
}
