package com.payu.stock.backend.apirest.payment.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    private String accountId;
    private String referenceCode;
    private String description;
    private String language;
    private String signature;
    private String notifyUrl;
    private AdditionalValues additionalValues;
    private Buyer buyer;
    private ShippingAddress shippingAddress;

    //Variables to be used when a refund is needed
    private String id;

}
