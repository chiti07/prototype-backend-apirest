package com.payu.stock.backend.apirest.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="products")
public class ProductEntity implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String name;
	private String description;
	private Double price; //BitDecimal
	private Integer amount;

	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="category_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})*/
	private String category;
	
	@Column(name="last_updated")
	@Temporal(TemporalType.DATE)
	private Date lastUpdated;

	private String picture;
	
	@PrePersist
	public void prePersist() {
		lastUpdated = new Date();
	}
			
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getCategoryEntity() {
		return category;
	}

	public void setCategoryEntity(String categoryEntity) {
		this.category = categoryEntity;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
}
