package com.payu.stock.backend.apirest.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

import com.payu.stock.backend.apirest.stock.UploadFile;
import com.payu.stock.backend.apirest.stock.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import com.payu.stock.backend.apirest.stock.ProductService;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/stock-api/products")
public class ProductRestController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UploadFile uploadFile;

    @GetMapping()
    public List<Product> index() {
        return productService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Product product = null;
        Map<String, Object> response = new HashMap<>();

        try {
            product = productService.findByID(id);
        } catch (DataAccessException e) {
            response.put("message", "Error doing the DB requestThe product: ");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


        if (product == null) {
            response.put("error", "The product does not exists");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping()
    public ResponseEntity<?> create(@RequestBody Product product) {
        Product newProduct = null;
        Map<String, Object> response = new HashMap<>();

        try {
            newProduct = productService.create(product);
        } catch (DataAccessException e) {
            response.put("message", "There was a problem creating the Product ");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "Product created successfully");
        response.put("product", newProduct);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product update(@RequestBody Product product, @PathVariable Long id) {
        Product currentProduct = productService.findByID(id);
        currentProduct.setName(product.getName());
        currentProduct.setDescription(product.getDescription());
        currentProduct.setAmount(product.getAmount());
        currentProduct.setPrice(product.getPrice());
        currentProduct.setCategory(product.getCategory());
        currentProduct.setLastUpdated(new Date());

        return productService.update(currentProduct);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        Product product = productService.findByID(id);
        String previousPictureName = product.getPicture();

        uploadFile.delete(previousPictureName);

        productService.delete(id);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id) {
        Map<String, Object> response = new HashMap<>();
        Product product = productService.findByID(id);

        if (!file.isEmpty()) {
            String fileName = null;

            try {
                fileName = uploadFile.copy(file);
            } catch (IOException e) {
                e.printStackTrace();
                response.put("message", "There was a problem uploading the Product picture ");
                response.put("error", e.getMessage());
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

            }

            String previousPictureName = product.getPicture();

            uploadFile.delete(previousPictureName);

            product.setPicture(fileName);
            productService.update(product);

            response.put("product", product);
            response.put("message", "Picture uploaded successfully");

        }

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

    }

    @GetMapping("/uploads/img/{pictureName:.+}")
    public ResponseEntity<Resource> seePicture(@PathVariable String pictureName) {
        Resource resource = null;

        try {
            resource = uploadFile.load(pictureName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");

        return new ResponseEntity<Resource>(resource, header, HttpStatus.OK);
    }


}
