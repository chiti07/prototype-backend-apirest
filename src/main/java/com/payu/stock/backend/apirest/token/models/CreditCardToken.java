package com.payu.stock.backend.apirest.token.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreditCardToken {
    private String payerId;
    private String name;
    private String identificationNumber;
    private String paymentMethod;
    private String number;
    private String expirationDate;
    private String creditCardTokenId;
    private String creationDate;
    private String maskedNumber;
    private String errorDescription;


}
