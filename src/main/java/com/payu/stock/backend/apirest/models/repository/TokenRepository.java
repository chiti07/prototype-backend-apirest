package com.payu.stock.backend.apirest.models.repository;

import com.payu.stock.backend.apirest.models.entity.TokenEntity;
import org.springframework.data.repository.CrudRepository;

public interface TokenRepository extends CrudRepository<TokenEntity, Long> {
}
