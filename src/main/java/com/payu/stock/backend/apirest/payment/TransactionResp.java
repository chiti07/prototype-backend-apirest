package com.payu.stock.backend.apirest.payment;


import com.payu.stock.backend.apirest.payment.models.TransactionResponse;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionResp {
    private String code;
    private String error;
    private TransactionResponse transactionResponse;
}
