package com.payu.stock.backend.apirest.user;

import com.payu.stock.backend.apirest.models.entity.User;

public interface UserServiceInterface {

    public User findByUsername(String username);
}
