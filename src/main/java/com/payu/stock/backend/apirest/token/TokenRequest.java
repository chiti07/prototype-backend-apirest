package com.payu.stock.backend.apirest.token;

import com.payu.stock.backend.apirest.token.models.CreditCardToken;
import com.payu.stock.backend.apirest.token.models.Merchant;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TokenRequest {

    private String command;
    private String language;
    private Merchant merchant;
    private CreditCardToken creditCardToken;

}
