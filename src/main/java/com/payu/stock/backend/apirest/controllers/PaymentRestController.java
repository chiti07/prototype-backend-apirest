package com.payu.stock.backend.apirest.controllers;


import com.payu.stock.backend.apirest.models.entity.TokenEntity;
import com.payu.stock.backend.apirest.models.repository.TokenRepository;
import com.payu.stock.backend.apirest.token.TokenFeignClient;
import com.payu.stock.backend.apirest.token.TokenRequest;
import com.payu.stock.backend.apirest.token.TokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("payment")
public class PaymentRestController {

    @Autowired
    private TokenFeignClient tokenFeignClient;

    @Autowired
    private TokenRepository tokenRepository;

    @PostMapping("/token")
    @ResponseStatus(HttpStatus.CREATED)
    public TokenResponse createToken(@RequestBody TokenRequest tokenRequest){
        Map<String, String> header = new HashMap<>();
        header.put("Accept", "application/json");

        return tokenFeignClient.createToken(header, tokenRequest);
    }
}
