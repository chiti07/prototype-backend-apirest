package com.payu.stock.backend.apirest.models.repository;

import com.payu.stock.backend.apirest.models.entity.PaymentEntity;
import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<PaymentEntity, Long> {
}
