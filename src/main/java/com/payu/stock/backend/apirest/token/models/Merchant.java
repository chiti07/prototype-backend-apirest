package com.payu.stock.backend.apirest.token.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Merchant {
    private String apiLogin;
    private String apiKey;
}
