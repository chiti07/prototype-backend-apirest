package com.payu.stock.backend.apirest.commons;

import com.payu.stock.backend.apirest.models.entity.TokenEntity;
import com.payu.stock.backend.apirest.token.TokenFeignClient;
import com.payu.stock.backend.apirest.token.TokenRequest;
import com.payu.stock.backend.apirest.token.TokenResponse;
import com.payu.stock.backend.apirest.token.models.CreditCardToken;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class MapperToken {

    @Autowired
    static TokenFeignClient tokenFeignClient;



    public static TokenEntity toTokenEntity(Map<String, String> header, TokenRequest tokenRequest){
        TokenEntity tokenEntity = new TokenEntity();
        TokenResponse tokenResponse = tokenFeignClient.createToken(header,tokenRequest);
        tokenEntity.setCreditCardTokenId(tokenResponse.getCreditCardToken().getCreditCardTokenId());
        tokenEntity.setCreationDate(tokenResponse.getCreditCardToken().getCreationDate());
        tokenEntity.setErrorDescription(tokenResponse.getCreditCardToken().getErrorDescription());
        tokenEntity.setExpirationDate(tokenResponse.getCreditCardToken().getExpirationDate());
        tokenEntity.setPayerId(tokenResponse.getCreditCardToken().getPayerId());
        tokenEntity.setPaymentMethod(tokenResponse.getCreditCardToken().getPaymentMethod());
        tokenEntity.setMaskedNumber(tokenResponse.getCreditCardToken().getMaskedNumber());
        tokenEntity.setName(tokenResponse.getCreditCardToken().getName());
        tokenEntity.setNumber(tokenResponse.getCreditCardToken().getNumber());
        tokenEntity.setIdentificationNumber(tokenResponse.getCreditCardToken().getIdentificationNumber());


        return tokenEntity;

    }

    public static TokenResponse toTokenResponse(TokenEntity tokenEntity){
        return TokenResponse.builder()
                .creditCardToken(CreditCardToken.builder()
                        .payerId(tokenEntity.getPayerId())
                        .name(tokenEntity.getName())
                        .identificationNumber(tokenEntity.getIdentificationNumber())
                        .paymentMethod(tokenEntity.getPaymentMethod())
                        .number(tokenEntity.getNumber())
                        .expirationDate(tokenEntity.getExpirationDate())
                        .creditCardTokenId(tokenEntity.getCreditCardTokenId())
                        .creationDate(tokenEntity.getCreationDate())
                        .maskedNumber(tokenEntity.getMaskedNumber())
                        .errorDescription(tokenEntity.getErrorDescription())
                        .build())
                .build();

    }

}
