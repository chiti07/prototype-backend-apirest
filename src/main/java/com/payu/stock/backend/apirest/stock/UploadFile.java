package com.payu.stock.backend.apirest.stock;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface UploadFile {

    public Resource load(String pictureName) throws MalformedURLException;

    public String copy(MultipartFile file) throws IOException;

    public boolean delete(String pictureName);

    public Path getPath(String pictureName);
}
