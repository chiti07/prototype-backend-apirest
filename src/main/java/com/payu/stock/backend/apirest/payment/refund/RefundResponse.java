package com.payu.stock.backend.apirest.payment.refund;

import com.payu.stock.backend.apirest.payment.models.TransactionResponse;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RefundResponse {
    private String code;
    private String error;
    private TransactionResponse transactionResponse;
}
