


INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Dumbbell Bars', '30 pounds', 50000, 10, '2021-01-01', '1');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Versalles Treadmills', '1HP 8Km', 250000, 10, '2021-01-01', '2');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Sportfit Multi-gym', '150 pounds', 700000, 10, '2021-01-01', '3');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Abdominal Wheel', '19cm of diameter', 35000, 10, '2021-01-01', '4');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Ball', 'Yoga ball', 15000, 10, '2021-01-01', '5');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Elastic Flat Bands', 'x3', 685000, 10, '2021-01-01', 'yoga');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Institutional Multi-gym', '300 pounds', 530000, 10, '2021-01-01', '3');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Parallel Bars', '38 cm width - 64 cm long', 80600, 10, '2021-01-01', 'abdominal');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Evo Treadmill', '2HP', 52000, 10, '2021-01-01', '2');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Multi-Strength Gym', '150 pounds', 450000, 10, '2021-01-01', '3');
INSERT INTO products(name, description, price, amount, last_updated, category) VALUES('Hex Dumbells', '20 pounds', 450000, 10, '2021-01-01', '1');


INSERT INTO users (username, password, enabled, name, last_name, email) VALUES ('juan', '$2a$10$Y67qVqrsp/jTh.W5CXOi7.vYjM54Jb8Aji0qhdsaaC9fiaTsb8Bom', true, 'Juan Luis', 'Chitiva Rivera', 'juan.chitiva@payu.com');
INSERT INTO users (username, password, enabled, name, last_name, email) VALUES ('admin', '$2a$10$psbeLlpBsOVYQJsrkBm2auHiRmZKLx47tNcZQ0c.mFceAAPuX2TWC', true, 'Admin', 'Gymapp', 'admin@gmail.com');

INSERT INTO roles (name) VALUES ('ROLE_USER');
INSERT INTO roles (name) VALUES ('ROLE_ADMIN');

INSERT INTO users_roles (user_id, role_id) VALUES (1,1);
INSERT INTO users_roles (user_id, role_id) VALUES (2,2);
INSERT INTO users_roles (user_id, role_id) VALUES (2,1);